﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    [SerializeField] private string tileTag;
    [SerializeField] private Vector3 tileSize;
    [SerializeField] private int maxDistance;
    private GameObject[] _tiles;
    
    
    void Start()
    {

        TextAsset t1 = (TextAsset) Resources.Load("terrainTiles", typeof(TextAsset));
        string s = t1.text;
        s = s.Replace("\n", ",");
        string[] subs = s.Split(',');

        for (int i = 0; i < 64; i = i+4)
        {
            Instantiate(Resources.Load(subs[i]), new Vector3(float.Parse(subs[i+1]), 
                float.Parse(subs[i+2]), float.Parse(subs[i+3])), Quaternion.identity);
        }
        
        this._tiles = GameObject.FindGameObjectsWithTag(tileTag);
        DeactivateDistantTiles();

    }
    
    void DeactivateDistantTiles()
    {
        Vector3 playerPosition = this.gameObject.transform.position;

        foreach (GameObject tile in _tiles)
        {
            Vector3 tilePosition = tile.gameObject.transform.position + (tileSize / 2f);

            float xDistance = Mathf.Abs(tilePosition.x - playerPosition.x);
            float zDistance = Mathf.Abs(tilePosition.z - playerPosition.z);
            

            if (xDistance + zDistance > maxDistance)
            {
                tile.SetActive(false);
            }
            else
            {
                tile.SetActive(true);
            }
        }
    }

    
    void Update()
    {
        DeactivateDistantTiles();
    }
}
