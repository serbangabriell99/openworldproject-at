﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AI_Movement : MonoBehaviour
{
    [SerializeField] private float speed = 10f;

    [SerializeField] private float waitTime = 3f;

    private float currentWaitTime = 0f;

    private float minX, maxX, minZ, maxZ;

    private Vector3 moveSpot;
    
    public Terrain terrain;
    
    public bool colliding = false;
    
    // Start is called before the first frame update
    void Start()
    {
        GetGroundSize();
        moveSpot = GetNewPosition();
    }

    // Update is called once per frame
    void Update()
    {
        WatchYourStep();
        GetToStepping();
    }

    private void GetGroundSize()
    {
        GameObject ground = GameObject.FindWithTag("Ground");
        Bounds worldBounds = new Bounds(terrain.terrainData.bounds.center + terrain.transform.position, terrain.terrainData.bounds.size);

        minX = (worldBounds.center.x - worldBounds.extents.x);
        maxX = (worldBounds.center.x + worldBounds.extents.x);
        minZ = (worldBounds.center.z - worldBounds.extents.z);
        maxZ = (worldBounds.center.z + worldBounds.extents.z);
        
    }

    Vector3 GetNewPosition()
    {
        float randomX = Random.Range(minX, maxX);
        float randomZ = Random.Range(minZ, maxZ);
        Vector3 newPosition = new Vector3(randomX, transform.position.y,randomZ);
        return newPosition;
    }

    void GetToStepping()
    {
        transform.position = Vector3.MoveTowards(transform.position, moveSpot, speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, moveSpot) <= 0.2f )
        {
            if (currentWaitTime <= 0)
            {
                moveSpot = GetNewPosition();
                currentWaitTime = waitTime;
            }
            else
            {
                currentWaitTime -= Time.deltaTime;
            }
        }else if (colliding)
        {
            moveSpot = GetNewPosition();
        }
    }

    void WatchYourStep()
    {
        Vector3 targetDirection = moveSpot - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.3f, 0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private void OnCollisionEnter(Collision other)
    {
        colliding = true;
    }
    
    private void OnCollisionExit(Collision other)
    {
        colliding = false;
    }
    
    
}
