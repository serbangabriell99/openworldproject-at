﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class RenderTheAssets : MonoBehaviour
{

    [SerializeField] private string tileTag;
    [SerializeField] private Vector3 tileSize;
    [SerializeField] private int maxDistance;
    private GameObject[] _tiles;
    string[] _tileNames =
    {
        "Tile1", "Tile2", "Tile3", "Tile4", "Tile5", "Tile6", "Tile7", "Tile8", "Tile9",
        "Tile10", "Tile11", "Tile12", "Tile13", "Tile14", "Tile15", "Tile16"
    };

    void SpawnObjects( string[] tileNames, int index)
    {
        Vector3 playerPosition = this.gameObject.transform.position;
        
        TextAsset t1 = (TextAsset) Resources.Load(tileNames[index], typeof(TextAsset));
        string s1 = t1.text;
        s1 = s1.Replace("\n", ",");
        string[] subs1 = s1.Split(',');
        
        
        for (int i = 0; i < subs1.Length; i = i+7)
        {
            float xDistance = Mathf.Abs(float.Parse(subs1[i+1]) + (tileSize.x / 2f) - playerPosition.x);
            float zDistance = Mathf.Abs(float.Parse(subs1[i+3]) + (tileSize.z / 2f) - playerPosition.z);

            if (GameObject.Find(subs1[i] + "(Clone)") == null)
            { 
                if (xDistance + zDistance < maxDistance)
                {
                    Instantiate(Resources.Load(subs1[i]), new Vector3(float.Parse(subs1[i+1]), float.Parse(subs1[i+2]), float.Parse(subs1[i+3])), Quaternion.Euler(float.Parse(subs1[i+4]),float.Parse(subs1[i+5]),float.Parse(subs1[i+6])));
                }
            }
            else
            {
                if (xDistance + zDistance > maxDistance)
                {
                    Destroy(GameObject.Find(subs1[i] + "(Clone)"));
                }
            }
        }
    }
    private void Update()
        {
            for (int i = 0; i < 16; i++)
            { 
                SpawnObjects(_tileNames, i);
            }
        }
}
    