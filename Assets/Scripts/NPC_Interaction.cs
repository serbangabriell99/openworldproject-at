﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NPC_Interaction : MonoBehaviour
{
    private bool inRange = false;
    private bool inChat = false;
    private bool inDialogue1 = true;
    private bool inDialogueLeftSubTree = false;
    private bool inDialogueUpSubTree = false;

    public GameObject npcWindow;
    public TextMeshProUGUI chatText;
    public TextMeshProUGUI leftText;
    public TextMeshProUGUI upText;
    public TextMeshProUGUI rightText;
    public string greeting;
    public string left1;
    public string leftResponse1;
    public string up1;
    public string up1Response1;
    public string right1;
    public string rightResponse1;

    public string left2;
    public string leftResponse2;
    public string up2;
    public string up1Response2;
    public string right2;
    public string rightResponse2;

    public string left3;
    public string leftResponse3;
    public string up3;
    public string up1Response3;
    public string right3;
    public string rightResponse3;

    public Camera main;
    public Camera AI;
    

    void Start()
    {
        inRange = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            if (inRange && !inChat)
            {
                npcWindow.gameObject.SetActive(true);
                chatText.GetComponent<TextMeshProUGUI>().text = greeting;
                loadDialogue1();
                main.gameObject.SetActive(false);
                AI.gameObject.SetActive(true);
                Cursor.visible = true;
                Screen.lockCursor = false;

            }
        }
    }

    void loadDialogue1()
    {
        inChat = true;
        inDialogue1 = true;
        inDialogueLeftSubTree = false;
        inDialogueUpSubTree = false;
        leftText.GetComponent<TextMeshProUGUI>().text = left1;
        upText.GetComponent<TextMeshProUGUI>().text = up1;
        rightText.GetComponent<TextMeshProUGUI>().text = right1;
    }
    
    void loadDialogueLeftSubTree()
    {
        inDialogue1 = false;
        inDialogueLeftSubTree = true;
        inDialogueUpSubTree = false;
        leftText.GetComponent<TextMeshProUGUI>().text = left2;
        upText.GetComponent<TextMeshProUGUI>().text = up2;
        rightText.GetComponent<TextMeshProUGUI>().text = right2;
    }
    void loadDialogueLeftSubTree2()
    {
        inDialogue1 = false;
        inDialogueLeftSubTree = false;
        inDialogueUpSubTree = false;
        leftText.GetComponent<TextMeshProUGUI>().text = "";
        upText.GetComponent<TextMeshProUGUI>().text = "";
    }
    void loadDialogueUpSubTree()
    {
        inDialogue1 = false;
        inDialogueLeftSubTree = false;
        inDialogueUpSubTree = true;
        leftText.GetComponent<TextMeshProUGUI>().text = left3;
        upText.GetComponent<TextMeshProUGUI>().text = up3;
        rightText.GetComponent<TextMeshProUGUI>().text = right3;
    }
    void loadDialogueUpSubTree2()
    {
        inDialogue1 = false;
        inDialogueLeftSubTree = false;
        inDialogueUpSubTree = false;
        leftText.GetComponent<TextMeshProUGUI>().text = "";
        upText.GetComponent<TextMeshProUGUI>().text = "";
    }

    public void Left()
    {
        if (inDialogue1)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = leftResponse1;
            loadDialogueLeftSubTree();
        }
        else if (inDialogueLeftSubTree)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = leftResponse2;
            loadDialogueLeftSubTree2();
        }
        else if (inDialogueUpSubTree)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = leftResponse3;
            loadDialogueUpSubTree2();
        }
    }

    public void Up()
    {
        if (inDialogue1)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = up1Response1;
            loadDialogueUpSubTree();
        }
        else if (inDialogueLeftSubTree)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = up1Response2;
            loadDialogueLeftSubTree2();
        }
        else if (inDialogueUpSubTree)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = up1Response3;
            loadDialogueUpSubTree2(); 
        }
    }

    public void Right()
    {
        CloseDialogue();
    }

    void CloseDialogue()
    {
        npcWindow.gameObject.SetActive(false);
        inChat = false;
        main.gameObject.SetActive(true);
        AI.gameObject.SetActive(false);
        Cursor.visible = false;
        Screen.lockCursor = true;
    }
    
}
