﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{
    public int maxHealth = 100;

    private int currentHealth;

    private Animator anim;

    private AI_Attack script;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        anim = gameObject.GetComponent<Animator>();
        script = GetComponent<AI_Attack>();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        anim.SetBool("dead",true);
        script.enabled = false;
        Debug.Log("Enemy Died!");
    }
    
    
}
