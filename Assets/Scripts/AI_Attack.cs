﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Attack : MonoBehaviour
{
    private Transform t;

    private Transform player;

    private float minPos = 15;

    private Animator anim;
    
    [SerializeField] private float speed = 1f;
    private bool followPlayer;
    private bool playerFound;
    private float timeBtwShots;
    public float startTimeBtwShots;
    public GameObject projectile;
    private Transform projectilePos;

    void Start()
    {
        t = this.transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = gameObject.GetComponent<Animator>();
        timeBtwShots = startTimeBtwShots;
        projectilePos = GameObject.FindGameObjectWithTag("Jaw").transform;

    }

    // Update is called once per frame
    void Update()
    {
        CheckingForPlayer();
        if(followPlayer) 
        {
            FollowPLayer();
        }

        if (followPlayer)
        {
          if (timeBtwShots <= 0)
          {
              anim.SetBool("attacking",true);
              StartCoroutine("ShootFireball");

          }
          else
          {
              timeBtwShots -= Time.deltaTime;
              anim.SetBool("attacking",false);
          }  
        }

    }

    void CheckingForPlayer()
    {
        if (Vector3.Distance(t.position, player.position) < minPos && !playerFound)
        {
            transform.LookAt(player);
           anim.SetBool("playerInRange",true);
           StartCoroutine("OnCompleteScreamAnimation");
           playerFound = true;
        }
    }

    void FollowPLayer()
    {
        if (Vector3.Distance(transform.position, player.position) > 8f)
        {
           // transform.LookAt(player);
           Rotate();
            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime); 
        }
        else if (Vector3.Distance(transform.position, player.position) < 8f && Vector3.Distance(transform.position, player.position) > 5f)
        {
            //transform.LookAt(player);
            Rotate();
            transform.position = this.transform.position;
        }
      /*  else if (Vector3.Distance(transform.position, player.position) < 5f)
        {
            //transform.LookAt(player);
            Rotate();
            transform.position = Vector3.MoveTowards(transform.position, player.position, -speed * Time.deltaTime); 
        }*/
       
    }
    IEnumerator OnCompleteScreamAnimation()
    {
        yield return new WaitForSeconds(4.5f);
        followPlayer = true;
        anim.SetBool("walking", true);
    }
    
    IEnumerator ShootFireball()
    {
        yield return new WaitForSeconds(.18f);
        Instantiate(projectile,projectilePos.transform.position,Quaternion.identity);
        timeBtwShots = startTimeBtwShots;
    }

    void Rotate()
    {
        Vector3 targetDirection = player.transform.position - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.06f, 0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }
    
}
