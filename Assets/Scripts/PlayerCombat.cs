﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    private Animator animator;

    public Transform sword;

    public float attackRange = 0.5f;

    public LayerMask enemyLayers;
    private bool attackin;

    public float attackRate = 3f;
    private float nextAttackTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextAttackTime)
        {
           if (Input.GetMouseButtonDown(0))
           {
               Attack();
               nextAttackTime = Time.time + 1f / attackRate;
           }  
        }
    }

    void Attack()
    {
        animator.SetTrigger("Attack");

        Collider[] hitEnemies = Physics.OverlapSphere(sword.position,attackRange,enemyLayers);

        foreach (Collider enemy in hitEnemies)
        {
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                Debug.Log("Point of contact: "+hit.point);
                Vector3 dir =  transform.position - hit.point    ;
                dir = -dir.normalized;
                enemy.gameObject.GetComponent<Rigidbody>().AddForce(dir * 160000); 
            }
            
            Debug.Log("We hit" + enemy.name);
            enemy.GetComponent<Dragon>().TakeDamage(20);
        }
        
    }

   /* void OnDrawGizmosSelected()
    {
        if (sword == null)
            return;
        Gizmos.DrawWireSphere(sword.position,attackRange);
    }*/
    
}
